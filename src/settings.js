import Vue from 'vue';
import axios from 'axios';

Vue.use(axios);

export const settings = {
	url: 'https://api.flickr.com/services/rest',
	apiKey: '4b19e67147361fc04df45fdc72f73462',
	method: 'flickr.photos.search',
	sortBy: 'relevance',
	extras: 'url_n',
	format: 'json',
	perPage: 10
};

export const appUrls = {
	fetchPhotos: (keyword, current) => {
		return axios.get(settings.url+
			'?method='+settings.method+
			'&sort='+settings.sortBy+
			'&api_key='+settings.apiKey+
			'&tags='+keyword+
			'&extras='+settings.extras+
			'&page='+current+
			'&format='+settings.format+
			'&nojsoncallback=1'+
			'&per_page='+settings.perPage);
	}
};